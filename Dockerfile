FROM maven:3-jdk-8

RUN java -version
RUN mvn --version

COPY . /devops-pipeline-assignment

WORKDIR /devops-pipeline-assignment

RUN mvn install
